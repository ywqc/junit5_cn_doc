
<h1 align="center">JUnit 5 用户指南中文版</h1>

<h1 align="center">Junit5 User Guide Chinese document</h1>

## 

### 版本 5.0.3

### 根据英文原版JUnit 5 User Guide的内容编写，适当加入了点自己的理解，如果觉得对你有帮助，请“star”下。

### 感谢英文版作者 Stefan Bechtold、Sam Brannen、Johannes Link、Matthias Merdes、Marc Philipp、Christian Stein 的辛劳付出

### 

### author: liushide 

### 
查看书籍地址： [JUnit 5 用户指南中文版](junit5UserGuide_zh_cn.md)

Copyright &copy; 2017-2018, liushide 刘士德 (liushide@qq.com)
中文著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

#### 如有问题请发邮件给我 liushide@qq.com