## 8. API改进

JUnit 5的主要目标之一是提高维护者的能力来改进JUnit，尽管它被应用于很多项目中。对于JUnit 4，很多原本是内部结构的东西都被外部扩展和工具生成器使用了。这使得更改JUnit 4特别困难，有时甚至是不可能更改的。

这就是为什么JUnit 5为所有公开可用的接口、类和方法引入了一个已定义的生命周期。

### 8.1. API版本和状态

每个已发布的产品都有一个版本号 `<major>.<minor>.<patch>`，所有公开可用的接口、类和方法都使用 [@API Guardian](https://github.com/apiguardian-team/apiguardian) 项目的 [@API](https://apiguardian-team.github.io/apiguardian/docs/current/api/) 进行注释。可以将注解的 `status`属性指定为以下值之一。

| 状态(status)     | 描述                                       |
| -------------- | ---------------------------------------- |
| `INTERNAL`     | 除了JUnit本身之外，任何代码都不能使用它。可能会在没有事先通知的情况下被删除。 |
| `DEPRECATED`   | 不应再使用;可能会在下一个小版本中消失。                     |
| `EXPERIMENTAL` | 用于新的实验特性，我们正在寻找反馈。谨慎使用此元素;它可能在将来被提升为  `MAINTAINED` 或  `STABLE` ，但也可能在没有事先通知的情况下被删除，甚至在补丁中删除。 |
| `MAINTAINED`   | 用于不以向后兼容方式更改的特性，至少是当前主要版本的下一个小版本。如果计划被移除，它将首先被降级为  `DEPRECATED` 。 |
| `STABLE`       | 用于在当前主要版本(5.*)中不会以向后不兼容方式更改的特性。          |

如果 `@API` 注解出现在类型上，那么它也被认为适用于该类型的所有公共成员。允许一个成员可以声明不同的 `status` 的低稳定性值。

### 8.2. 实验性的api

下表列出了哪些api目前被指定为实验性的(通过 `@API(status = EXPERIMENTAL)` 指定)。在采用这些api时应该谨慎。

| Package Name                             | Class Name                    | Type         |
| ---------------------------------------- | ----------------------------- | ------------ |
| `org.junit.jupiter.api`                  | `DynamicContainer`            | `class`      |
| `org.junit.jupiter.api`                  | `DynamicNode`                 | `class`      |
| `org.junit.jupiter.api`                  | `DynamicTest`                 | `class`      |
| `org.junit.jupiter.api`                  | `TestFactory`                 | `annotation` |
| `org.junit.jupiter.migrationsupport.rules` | `EnableRuleMigrationSupport`  | `annotation` |
| `org.junit.jupiter.migrationsupport.rules` | `ExpectedExceptionSupport`    | `class`      |
| `org.junit.jupiter.migrationsupport.rules` | `ExternalResourceSupport`     | `class`      |
| `org.junit.jupiter.migrationsupport.rules` | `VerifierSupport`             | `class`      |
| `org.junit.jupiter.params`               | `ParameterizedTest`           | `annotation` |
| `org.junit.jupiter.params.converter`     | `ArgumentConversionException` | `class`      |
| `org.junit.jupiter.params.converter`     | `ArgumentConverter`           | `interface`  |
| `org.junit.jupiter.params.converter`     | `ConvertWith`                 | `annotation` |
| `org.junit.jupiter.params.converter`     | `JavaTimeConversionPattern`   | `annotation` |
| `org.junit.jupiter.params.converter`     | `SimpleArgumentConverter`     | `class`      |
| `org.junit.jupiter.params.provider`      | `Arguments`                   | `interface`  |
| `org.junit.jupiter.params.provider`      | `ArgumentsProvider`           | `interface`  |
| `org.junit.jupiter.params.provider`      | `ArgumentsSource`             | `annotation` |
| `org.junit.jupiter.params.provider`      | `ArgumentsSources`            | `annotation` |
| `org.junit.jupiter.params.provider`      | `CsvFileSource`               | `annotation` |
| `org.junit.jupiter.params.provider`      | `CsvSource`                   | `annotation` |
| `org.junit.jupiter.params.provider`      | `EnumSource`                  | `annotation` |
| `org.junit.jupiter.params.provider`      | `MethodSource`                | `annotation` |
| `org.junit.jupiter.params.provider`      | `ValueSource`                 | `annotation` |
| `org.junit.jupiter.params.support`       | `AnnotationConsumer`          | `interface`  |
| `org.junit.platform.gradle.plugin`       | `EnginesExtension`            | `class`      |
| `org.junit.platform.gradle.plugin`       | `FiltersExtension`            | `class`      |
| `org.junit.platform.gradle.plugin`       | `JUnitPlatformExtension`      | `class`      |
| `org.junit.platform.gradle.plugin`       | `JUnitPlatformPlugin`         | `class`      |
| `org.junit.platform.gradle.plugin`       | `PackagesExtension`           | `class`      |
| `org.junit.platform.gradle.plugin`       | `SelectorsExtension`          | `class`      |
| `org.junit.platform.gradle.plugin`       | `TagsExtension`               | `class`      |
| `org.junit.platform.surefire.provider`   | `JUnitPlatformProvider`       | `class`      |

### 8.3. @API 工具支持

 [@API Guardian](https://github.com/apiguardian-team/apiguardian) 项目计划为使用 [@API](https://apiguardian-team.github.io/apiguardian/docs/current/api/)注解的api的发布者和消费者提供工具支持。例如，工具支持可能提供一种方法来检查JUnit api是否按照 `@API` 注释声明使用。

